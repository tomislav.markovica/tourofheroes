import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DetailComponent } from './components/detail/detail.component';
import { HeroesComponent } from './components/heroes/heroes.component';
import { HeroDetailComponent } from './components/hero-detail/hero-detail.component';
import { MessagesComponent } from './components/messages/messages.component';

import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './services/inMemoryData/in-memory-data.service';
import { HeroSearchComponent } from './components/hero-search/hero-search.component';
import { HeroFormComponent } from './components/hero-form/hero-form.component';
import { NgOptimizedImage, provideImgixLoader } from '@angular/common';
import { ReactiveFormComponent } from './components/reactive-form/reactive-form.component';
import { ReactiveFormInTemplateComponent } from './components/reactive-form-in-template/reactive-form-in-template.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    DetailComponent,
    HeroesComponent,
    HeroDetailComponent,
    MessagesComponent,
    HeroSearchComponent,
    HeroFormComponent,
    ReactiveFormComponent,
    ReactiveFormInTemplateComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    ),
    NgOptimizedImage,
    ReactiveFormsModule
  ],
  providers: [
    provideImgixLoader('https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Shaqi_jrvej.jpg/1200px-Shaqi_jrvej.jpg')
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
