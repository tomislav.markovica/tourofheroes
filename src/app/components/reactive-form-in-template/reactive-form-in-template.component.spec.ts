import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReactiveFormInTemplateComponent } from './reactive-form-in-template.component';

describe('ReactiveFormInTemplateComponent', () => {
  let component: ReactiveFormInTemplateComponent;
  let fixture: ComponentFixture<ReactiveFormInTemplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReactiveFormInTemplateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReactiveFormInTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
