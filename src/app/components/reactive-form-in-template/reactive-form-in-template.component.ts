import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactive-form-in-template',
  /* templateUrl: './reactive-form-in-template.component.html',
  styleUrls: ['./reactive-form-in-template.component.scss'] */
  template: `
    <form [formGroup]="form" (ngSubmit)="onSubmit()">
      <div *ngIf="first.invalid"> Name is too short. </div>
      <div *ngIf="email.invalid"> E-mail address not in correct form. </div>

      <input formControlName="first" placeholder="First name">
      <input formControlName="last" placeholder="Last name">
      <input formControlName="email" placeholder="E-mail">

      <button type="submit">Submit</button>
   </form>
   <button (click)="setValue()">Set preset value</button>
  `,
})
export class ReactiveFormInTemplateComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  form = new FormGroup({
    first: new FormControl('Nancy', Validators.minLength(2)),
    last: new FormControl('Drew'),
    email: new FormControl('', Validators.email || Validators.minLength(2))
  });

  get first(): any {
    return this.form.get('first');
  }

  get email(): any {
    return this.form.get('email');
  }

  onSubmit(): void {
    console.log(this.form.value);  // {first: 'Nancy', last: 'Drew'}
  }

  setValue() {
    this.form.setValue({first: 'Carson', last: 'Drew', email: 'tm@em2.hr'});
  }

}
